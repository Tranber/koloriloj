# Koloriloj

[En cours de développement]

Librairie de traitement de couleur :

- Parsing valeurs (html, css...)
- Conversion de modèles
- Opérations (teintes, mélange...)

## Longueurs d'ondes standardisées

- red : 700 nm
- green : 546.1 nm
- blue : 435.8 nm

voir : [CIE 1931 color space](https://en.wikipedia.org/wiki/CIE_1931_color_space)
