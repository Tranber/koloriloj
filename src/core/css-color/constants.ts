import {
  KeyedConfig,
  Nullable,
  Unknown,
  NumberValue,
  IndexedConfig,
  ArrayOrSingle
} from '../../types';
import { ColorModel, RGBColor, HSLColor, HWBColor } from '../color';
import { CSSUnitValue } from '../css';

enum ValueType {
  INT_BYTE = 1,
  FLOAT_NORMALIZED = 2,
  PERCENT = 4,
  FLOAT_ANGLE = 8,
  FLOAT_DEGREE = 16,
  FLOAT_GRAD = 32,
  FLOAT_RADIAN = 64,
  FLOAT_TURN = 128
}

enum UnitType {
  NONE = 1,
  PERCENT,
  DEGREE,
  GRAD,
  RADIAN,
  TURN
}

enum ValueOperation {
  MOD = 1,
  CLAMP
}

interface UnitConfig {
  label: string;
}

const UnitConfigs: IndexedConfig<UnitConfig> = {
  [UnitType.DEGREE]: {
    label: 'deg'
  }
};

interface ValueConfig {
  unit: string | number;
  min: number;
  max: number;
  operation?: number;
}

const ValueTypeConfigs: IndexedConfig<ValueConfig> = {
  [ValueType.INT_BYTE]: {
    min: 0,
    max: 255,
    unit: UnitType.NONE
  },
  [ValueType.PERCENT]: {
    min: 0,
    max: 100,
    unit: '%'
  },
  [ValueType.FLOAT_NORMALIZED]: {
    min: 0,
    max: 1,
    unit: UnitType.NONE
  },
  [ValueType.FLOAT_ANGLE]: {
    min: 0,
    max: 360,
    unit: UnitType.NONE
  },
  [ValueType.FLOAT_DEGREE]: {
    min: 0,
    max: 360,
    unit: 'deg'
  },
  [ValueType.FLOAT_GRAD]: {
    min: 0,
    max: 400,
    unit: 'grad'
  },
  [ValueType.FLOAT_RADIAN]: {
    min: 0,
    max: Math.PI * 2,
    unit: 'rad'
  },
  [ValueType.FLOAT_TURN]: {
    min: 0,
    max: 1,
    unit: 'turn'
  }
};

function getValueConfig(valueType: number): Nullable<ValueConfig> {
  if (Reflect.has(ValueTypeConfigs, valueType)) {
    return Reflect.get(ValueTypeConfigs, valueType);
  }

  return null;
}

class TokenPattern {
  static INT = '\\d';
  static FLOAT = '[+-]?(?:\\d*\\.)?\\d+(?:[eE]\\d+)?';
  static HEXADECIMAL = '[0-9a-f]';
  static NORMALIZEDFLOAT = '1|0|0?\\.\\d+';
}

class ValuePattern {
  static PERCENT = `(${TokenPattern.FLOAT}%)`;
  static ANGLE = `(${TokenPattern.FLOAT})(deg|rad|grad|turn)`; // see math number
  static HEXADECIMAL = `${TokenPattern.HEXADECIMAL}{1,2}`; // see math number
  static INTCOLOR = `${TokenPattern.INT}{1,3}`;
  static ALPHA = `(?:${TokenPattern.NORMALIZEDFLOAT})|(?:${TokenPattern.FLOAT})`;
  static VALUE = `(${TokenPattern.FLOAT})(%|deg|rad|grad|turn)?`;
  static MODEL = '(?:rgb)|(?:hsl)|(?:hwb)|(?:lab)|(?:lch)|(?:device\\-cmyk)';
}

class RegExpSegment {
  static COLORVALUE = `(${ValuePattern.INTCOLOR}|(?:${TokenPattern.FLOAT}%))`;
  static COLORSEPARATOR_LEGACY = '(?:(?:\\s+)|(?:\\s*,\\s*))';
  static COLORSEPARATOR = '\\s+';
  static ALPHAVALUE = `(?:\\s*(?:\\/|,)\\s*(${ValuePattern.ALPHA}%)?)?`;
  static HUEVALUE = `(${TokenPattern.FLOAT}(?:deg|rad|grad|turn)?)`;
}

const rgbColorTokens = [
  '\\(',
  RegExpSegment.COLORVALUE,
  RegExpSegment.COLORSEPARATOR_LEGACY,
  RegExpSegment.COLORVALUE,
  RegExpSegment.COLORSEPARATOR_LEGACY,
  RegExpSegment.COLORVALUE,
  RegExpSegment.ALPHAVALUE,
  '\\)'
];

const hslColorTokens = [
  '\\(',
  RegExpSegment.HUEVALUE,
  RegExpSegment.COLORSEPARATOR_LEGACY,
  ValuePattern.PERCENT,
  RegExpSegment.COLORSEPARATOR_LEGACY,
  ValuePattern.PERCENT,
  RegExpSegment.ALPHAVALUE,
  '\\)'
];

const hwbColorTokens = [
  '\\(',
  RegExpSegment.HUEVALUE,
  RegExpSegment.COLORSEPARATOR,
  ValuePattern.PERCENT,
  RegExpSegment.COLORSEPARATOR,
  ValuePattern.PERCENT,
  RegExpSegment.ALPHAVALUE,
  '\\)'
];

class ColorPattern {
  static HEXADECIMAL = new RegExp(
    `#(${TokenPattern.HEXADECIMAL}{3}|${TokenPattern.HEXADECIMAL}{6}|${TokenPattern.HEXADECIMAL}{4}|${TokenPattern.HEXADECIMAL}{8})`,
    'i'
  );
  static RGB = new RegExp(`rgba?${rgbColorTokens.join('')}`);
  static HSL = new RegExp(`hsla?${hslColorTokens.join('')}`);
  static HWB = new RegExp(`hwb${hwbColorTokens.join('')}`);
}

function getUnitValue(value: Unknown<NumberValue>): Nullable<CSSUnitValue> {
  if (typeof value === 'number') {
    value = value.toString();
  }
  if (typeof value === 'string') {
    const valueParts = value.match(ValuePattern.VALUE);
    if (valueParts) {
      return new CSSUnitValue(Number.parseFloat(valueParts[1]), valueParts[2]);
    }
  }

  return null;
}

interface CSSColorConfig {
  model: number;
  class: Nullable<any>;
  pattern: Nullable<RegExp>;
  channels: ArrayOrSingle<ValueType>[];
}

// INT_BYTE
//   FLOAT_NORMALIZED
//   PERCENT
//   FLOAT_ANGLE
//   FLOAT_DEGREE
//   FLOAT_GRAD
//   FLOAT_RADIAN
//   FLOAT_TURN
const ChannelType = {
  COLOR: [ValueType.INT_BYTE, ValueType.PERCENT],
  ALPHA: [ValueType.FLOAT_NORMALIZED, ValueType.PERCENT],
  HUE: [
    ValueType.FLOAT_ANGLE,
    ValueType.FLOAT_DEGREE,
    ValueType.FLOAT_GRAD,
    ValueType.FLOAT_RADIAN,
    ValueType.FLOAT_TURN
  ]
};

const CSSColorKeys: KeyedConfig<CSSColorConfig> = {
  rgb: {
    model: ColorModel.RGB,
    class: RGBColor,
    pattern: ColorPattern.RGB,
    channels: [
      ChannelType.COLOR,
      ChannelType.COLOR,
      ChannelType.COLOR,
      ChannelType.ALPHA
    ]
  },
  hsl: {
    model: ColorModel.HSL,
    class: HSLColor,
    pattern: ColorPattern.HSL,
    channels: [
      ChannelType.HUE,
      ValueType.PERCENT,
      ValueType.PERCENT,
      ChannelType.ALPHA
    ]
  },
  hwb: {
    model: ColorModel.HWB,
    class: HWBColor,
    pattern: ColorPattern.HWB,
    channels: [
      ChannelType.HUE,
      ValueType.PERCENT,
      ValueType.PERCENT,
      ChannelType.ALPHA
    ]
  },
  lab: {
    model: ColorModel.LAB,
    class: null,
    pattern: null,
    channels: []
  },
  lch: {
    model: ColorModel.LCH,
    class: null,
    pattern: null,
    channels: []
  },
  'device-cmyk': {
    model: ColorModel.CMYK,
    class: null,
    pattern: null,
    channels: []
  }
};

function getModelByKey(key: string): Nullable<CSSColorConfig> {
  if (Reflect.has(CSSColorKeys, key)) {
    return Reflect.get(CSSColorKeys, key);
  }

  return null;
}

export {
  // Units
  ValueType,
  getValueConfig,
  // Patterns
  ColorPattern,
  ValuePattern,
  TokenPattern,
  getModelByKey,
  getUnitValue
};
