import {
  ValuePattern,
  ValueType,
  getValueConfig,
  getModelByKey,
  getUnitValue
} from './constants';
import { Color, Nullable, NumberValue, ArrayOrSingle } from '../../types';

function formatHueValue(
  hueValue: number,
  hueFormat: number,
  hueType = ValueType.FLOAT_DEGREE
): string {
  const { max: circleLength, unit } = getValueConfig(hueType);
  if (hueFormat !== ValueType.FLOAT_ANGLE) {
    return `${hueValue * circleLength}${unit}`;
  }

  return `${hueValue * circleLength}`;
}

function formatColorValue(colorValue: number, colorFormat: number): string {
  if (colorFormat === ValueType.PERCENT) {
    return `${colorValue * 100}%`;
  }

  return `${colorValue * 255}`;
}

function formatAlphaValue(alphaValue: number, alphaFormat: number) {
  if (alphaFormat === ValueType.PERCENT) {
    return `${alphaValue * 100}%`;
  }

  return alphaValue;
}

function convertValue(nVal: NumberValue): number {
  if (typeof nVal === 'number') {
    return nVal;
  } else if (typeof nVal === 'string') {
    const angleValue = nVal.match(ValuePattern.ANGLE);
    const percentValue = nVal.match(ValuePattern.PERCENT);
    const hexadecimalValue = nVal.match(ValuePattern.HEXADECIMAL);

    if (angleValue && angleValue.length >= 2) {
      return Number.parseFloat(angleValue[1]);
    } else if (percentValue && percentValue.length >= 2) {
      return Number.parseFloat(percentValue[1]);
    } else if (hexadecimalValue && hexadecimalValue.length >= 1) {
      return Number.parseInt(hexadecimalValue[0], 16);
    }
  }
}

function toNormalizedNumber(nVal: NumberValue, min = 0, max = 1): number {
  const delta = max - min;
  const numValue = convertValue(nVal);
  if (numValue >= max) {
    return 1;
  } else if (numValue <= min) {
    return 0;
  } else {
    if (delta === 0) {
      throw new Error('Division by zero');
    } else {
      return numValue / delta;
    }
  }
}

function normalizeValues(
  values: NumberValue[],
  valuesTypes: ArrayOrSingle<ValueType>[]
): Nullable<number>[] {
  return values.map((value, index) => {
    const valueType = valuesTypes[index] as number;
    const valueConfig = getValueConfig(valueType);
    const valueData = getUnitValue(value);
    console.log('value : ', value, valueType, valueConfig, valueData);
    if (valueConfig && value) {
      console.log('value data : ', value, valueType, valueData);
      return toNormalizedNumber(value, valueConfig.min, valueConfig.max);
    }
    return null;
  });
}

function parse(cssString: string): Nullable<Color> {
  // TODO implementation
  console.log('color string : ', cssString);
  const modelResult = cssString.match(ValuePattern.MODEL);
  // console.log("color model : ", modelResult);
  if (modelResult && modelResult.length >= 1) {
    const modelConfig = getModelByKey(modelResult[0]);
    console.log('color model config : ', modelConfig);
    if (modelConfig) {
      const colorResult = cssString.match(modelConfig.pattern);
      console.log('color result : ', colorResult);
      if (colorResult) {
        const colorChannels = colorResult.splice(1);

        if (colorChannels) {
          const normalizedChannels = normalizeValues(
            colorChannels,
            modelConfig.channels
          );

          console.log('color value : ', colorChannels, normalizedChannels);
        }

        return new modelConfig.class(...colorChannels);
      }
    }
  }

  return null;
}

export { formatHueValue, formatColorValue, formatAlphaValue, parse };
