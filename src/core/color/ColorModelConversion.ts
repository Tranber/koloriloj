import RGBColor from './RGBColor';
import HSLColor from './HSLColor';
import HWBColor from './HWBColor';
import { hueFromRGB, tempRGBfromHue } from './utils';
import { Color } from '../../types';

class ColorModelConversion {
  static RGBToHSL(color: RGBColor): HSLColor {
    if (color instanceof RGBColor) {
      const { r, g, b, a } = color;
      const max = Math.max(r, g, b);
      const min = Math.min(r, g, b);

      let hue = hueFromRGB(r, g, b, max, min);
      if (hue < 0) {
        hue += 360;
      }

      let saturation = 0;
      if (max !== 0 && min !== 1) {
        saturation = (max - min) / (1 - Math.abs(max + min - 1));
      }

      const lightness = (max + min) / 2;

      return new HSLColor(hue, saturation, lightness, a);
    } else {
      throw new TypeError('Input should be RGB color');
    }
  }

  static HSLToRGB(color: HSLColor): RGBColor {
    if (color instanceof HSLColor) {
      const { h, s, l, a } = color;
      const hue = h * 360;
      const chroma = (1 - Math.abs(2 * l - 1)) * s;

      const tempHue = hue / 60;

      const { r: tR, g: tG, b: tB } = tempRGBfromHue(tempHue, chroma);

      const m = l - chroma / 2;

      const r = tR + m;
      const g = tG + m;
      const b = tB + m;

      return new RGBColor(r, g, b, a);
    } else {
      throw new TypeError('Input should be HSL color');
    }
  }

  static HWBToRGB(color: HWBColor): RGBColor {
    if (color instanceof HWBColor) {
      let rgbColor = this.HSLToRGB(new HSLColor(color.h, 1, 0.5, color.a));
      const chroma = rgbColor.rgb;

      for (let k in chroma) {
        rgbColor[k] = chroma[k] * (1 - color.w - color.b) + color.w;
      }

      return rgbColor;
    } else {
      throw new TypeError('Input should be HWB color');
    }
  }

  static RGBToHWB(color: RGBColor): HWBColor {
    if (color instanceof RGBColor) {
      let hslColor = this.RGBToHSL(color);
      let white = Math.min(...color.rgb);
      let black = 1 - Math.max(...color.rgb);

      return new HWBColor(hslColor.h, white, black, hslColor.a);
    } else {
      throw new TypeError('Input should be RGB color');
    }
  }

  static toRGBColor(color: Color): RGBColor {
    if (color instanceof HSLColor) {
      return this.HSLToRGB(color);
    } else if (color instanceof HWBColor) {
      return this.HWBToRGB(color);
    } else if (color instanceof RGBColor) {
      return color;
    } else {
      throw new TypeError('Input has unknown color type');
    }
  }
}

export default ColorModelConversion;
