function hueFromRGB(
  r: number,
  g: number,
  b: number,
  maxRGB: number,
  minRGB: number
) {
  if (maxRGB === minRGB) {
    return 0;
  }

  const delta = maxRGB - minRGB;

  if (maxRGB === r) {
    return 60 * ((g - b) / delta);
  }
  if (maxRGB === g) {
    return 60 * (2 + (b - r) / delta);
  }
  if (maxRGB === b) {
    return 60 * (4 + (r - g) / delta);
  }
}

function tempRGBfromHue(tHue: number, chroma: number) {
  const tColor = chroma * (1 - Math.abs((tHue % 2) - 1));

  if (0 <= tHue && tHue <= 1) {
    return { r: chroma, g: tColor, b: 0 };
  }
  if (1 <= tHue && tHue <= 2) {
    return { r: tColor, g: chroma, b: 0 };
  }
  if (2 <= tHue && tHue <= 3) {
    return { r: 0, g: chroma, b: tColor };
  }
  if (3 <= tHue && tHue <= 4) {
    return { r: 0, g: tColor, b: chroma };
  }
  if (4 <= tHue && tHue <= 5) {
    return { r: tColor, g: 0, b: chroma };
  }
  if (5 <= tHue && tHue <= 6) {
    return { r: chroma, g: tHue, b: tColor };
  }
  return { r: 0, g: 0, b: 0 };
}

export { hueFromRGB, tempRGBfromHue };
