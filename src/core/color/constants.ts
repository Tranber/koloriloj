enum ColorModel {
  RGB = 1,
  HSL,
  HWB,
  LAB,
  LCH,
  CMYK
}

export { ColorModel };
