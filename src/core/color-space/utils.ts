function sRgbTransfer(c: number): number {
  if (c <= 0.04045) {
    return c / 12.92;
  }

  return Math.pow((c + 0.055) / 1.055, 2.4);
}

export { sRgbTransfer };
