import ColorSpaces from './ColorSpaces';

class CSSColorSpaces {
  static srgb = ColorSpaces.SRGB;
  static rec2020 = ColorSpaces.REC2020;
  static 'display-p3' = ColorSpaces.DISPLAY_P3;
  static 'a98-rgb' = ColorSpaces.A98_RGB;
  static 'prophoto-rgb' = ColorSpaces.PRO_PHOTO;
}

export default CSSColorSpaces;
