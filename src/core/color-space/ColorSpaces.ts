import { sRgbTransfer } from './utils';

class ColorSpaces {
  static SRGB = {
    r: {
      x: 0.64,
      y: 0.33
    },
    g: {
      x: 0.3,
      y: 0.6
    },
    b: {
      x: 0.15,
      y: 0.06
    },
    w: {
      x: 0.3127,
      y: 0.329
    },
    t: sRgbTransfer
  };

  static DISPLAY_P3 = {
    r: {
      x: 0.68,
      y: 0.32
    },
    g: {
      x: 0.265,
      y: 0.69
    },
    b: {
      x: 0.15,
      y: 0.06
    },
    w: {
      x: 0.3127,
      y: 0.329
    },
    t: sRgbTransfer
  };

  static REC2020 = {
    r: {
      x: 0.708,
      y: 0.292
    },
    g: {
      x: 0.17,
      y: 0.797
    },
    b: {
      x: 0.131,
      y: 0.046
    },
    w: {
      x: 0.3127,
      y: 0.329
    },
    t: () => 1 / 2.4
  };

  static A98_RGB = {
    r: {
      x: 0.64,
      y: 0.33
    },
    g: {
      x: 0.21,
      y: 0.71
    },
    b: {
      x: 0.15,
      y: 0.06
    },
    w: {
      x: 0.3127,
      y: 0.329
    },
    t: () => 256 / 563
  };
  static PRO_PHOTO = {
    r: {
      x: 0.7347,
      y: 0.2653
    },
    g: {
      x: 0.1596,
      y: 0.8404
    },
    b: {
      x: 0.0366,
      y: 0.0001
    },
    w: {
      x: 0.3457,
      y: 0.3585
    },
    t: () => 1 / 1_800
  };
}

export default ColorSpaces;
