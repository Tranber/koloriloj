type NumberValue = number | string;
type Nullable<T> = T | null;
type Unknown<T> = T | undefined;
type ArrayOrSingle<T> = T[] | T;

interface KeyedConfig<T> {
  [key: string]: T;
}

interface IndexedConfig<T> {
  [key: number]: T;
}

export {
  NumberValue,
  Nullable,
  Unknown,
  KeyedConfig,
  IndexedConfig,
  ArrayOrSingle
};
