import { RGBColor, HSLColor, HWBColor } from '../core';

type Color = RGBColor | HSLColor | HWBColor;

export { Color };
