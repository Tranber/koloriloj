const path = require('path');

const libDir = '../build/lib/';

const {
  ValuePattern,
  ColorPattern,
  TokenPattern,
  parse
} = require(path.resolve(__dirname, libDir, 'color-utils.js'));

// const result = "hsl(45grad, 12% .85% / .5%)".match(ValuePattern.MODEL);
// console.log(
//   "value pattern : ",
//   result
//   // result && result.length,
//   // ColorPattern.CSS_COLOR
// );

// const resultRgb = "rgba(45, 12, 23, .5%)".match(ValuePattern.MODEL);
// console.log(
//   "rgb pattern : ",
//   resultRgb
//   // resultRgb && resultRgb.length,
//   // ColorPattern.CSS_COLOR
// );

// console.log('color patterns : ', ColorPattern.RGB_NUMBER)

// "hsl(5deg 42% 87%)"
// rgb(45, 4, 34)

const resultColor = parse('rgb(45%, 4, 34)');
// const resultColor = parse('hsl(10deg, 4%, 34%)');

console.log('result rgb color : ', resultColor);
